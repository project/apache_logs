# Apache Logs

Apache Logs module is developed for helping you to check the apache
logs when you are encountering website crash or white screen of death.
Designed for users unfamiliar with Drupal, it enables viewing Apache
logs on a dedicated URL. This solution ensures quick error
identification without the need for direct server access.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/apache_logs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/apache_logs).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Similar Projects
- Maintainers

## Requirements

This module is independent of all contrib modules and operates
within Drupal core. No additional dependencies are needed for
basic functionality.

## Recommended modules

There are no dependencies for this module and all the features
would be extended in this module itself. Thus, no additional
modules or libraries are required for this standalone module.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
Upon installation, this Drupal module helps you to check apache logs
when encountered website crashes. Designed for users unfamiliar with
Drupal, it enables viewing Apache logs on a dedicated URL. This solution
ensures quick error identification without the need for direct server
access.

## Configuration

No Configuration is required.

## Similar Projects
While an older Drupal 7 module exists, this one differentiates itself by
supporting the latest Drupal versions (9 and 10). The alternative module
lacks updates since 2013.

## Maintainers

Current maintainers:

- [Urvashi Vora (urvashi_vora)](https://www.drupal.org/u/urvashi_vora)
- [Dhruvesh Tripathi (dhruveshdtripathi)](https://www.drupal.org/u/dhruveshdtripathi)

Supporting organizations:

- [atlas-softweb-pvt-ltd](https://www.drupal.org/atlas-softweb-pvt-ltd) Created and sponsored this module and supported in the module development.
